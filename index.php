<?php
  require 'con.php';
  require 'functions.php';

  $query_status = "SELECT distinct * FROM status WHERE lang = 'en'";
  $query_type = "SELECT distinct * FROM types WHERE lang = 'en'";
  $query_features = "SELECT distinct * FROM features WHERE lang = 'en' LIMIT 20";

?>
<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Tue Jan 23 2018 22:29:19 GMT+0000 (UTC)  -->
<html>
<head>
  <meta charset="utf-8">
  <title>SB Map Properties</title>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/sb-map-properties.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Raleway:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="map-top-container-search">
    <a href="#google-map.js" class="map-btn-close-search w-inline-block">
      <div>X</div>
    </a>
    <div class="map-top-brand"><img src="images/logo-sb.png"></div>
    <div class="map-top-search">
      <form action="" id="search-form" name="search-form" data-name="Search Form" class="map-top-search-form" method="POST">
        <input type="text" class="map-search-word w-input" maxlength="256" name="search-word" data-name="search-word" placeholder="Search Word" id="search-word">
        <div class="map-select-search-status">
          <input type="text" class="map-select-status w-input" maxlength="256" name="status" data-name="status" data-ix="map-input-init" placeholder="Sale / Rent" id="status" value="">
          <div class="map-input-selection">
            <ul class="map-input-list">
              <?php

              $res_status = $con->query($query_status);

              if (!$res_status) {
                trigger_error('Invalid query: ' . $con->error);
              }

              if ($res_status->num_rows) {
                while ($row_status = $res_status->fetch_object()) {
              ?>
                <li class="map-input-list-item">
                  <div class="map-input-list-checkbox-wrapper w-checkbox">
                    <input type="checkbox" id="<?=$row_status->title?>" class="map-input-list-checkbox w-checkbox-input" value="<?=$row_status->title?>">
                    <label for="<?=$row_status->title?>" class="map-input-list-checkbox-label w-form-label"><?=$row_status->title?></label>
                  </div>
                </li>
              <?php
                }
              }
              $res_status->free();
              ?>
            </ul>
          </div>
        </div>
        <div class="map-select-search-type">
          <input type="text" class="map-select-type w-input" maxlength="256" name="type" data-name="type" data-ix="map-input-type" placeholder="Type" id="type">
          <div class="map-input-selection-type">
              <?php

              $res_type = $con->query($query_type);

              if (!$res_type) {
                trigger_error('Invalid query: ' . $con->error);
              }

              if ($res_type->num_rows) {
                while ($row_type = $res_type->fetch_object()) {
              ?>
              <li class="map-input-list-item">
                <div class="map-input-list-checkbox-wrapper w-checkbox">
                  <input type="checkbox" id="<?=$row_type->title?>" class="map-input-list-checkbox w-checkbox-input" value="<?=$row_type->title?>">
                  <label for="<?=$row_type->title?>" class="map-input-list-checkbox-label w-form-label"><?=$row_type->title?></label>
                </div>
              </li>
              <?php
                }
              }
              $res_type->free();
              ?>
          </div>
        </div>
        <input type="text" class="map-input-search-beds w-input" maxlength="256" name="beds" data-name="beds" placeholder="Beds" id="beds">
        <input type="text" class="map-input-search-baths w-input" maxlength="256" name="baths" data-name="baths" placeholder="Baths" id="baths">
        <div class="map-select-search-amenities">
          <input type="text" class="map-select-amenities w-input" maxlength="256" name="amenities" data-name="amenities" data-ix="map-input-amenities" placeholder="Amenities" id="amenities">
          <div class="map-input-selection-amenities">
            <?php

            $res_feature = $con->query($query_features);

            if (!$res_feature) {
              trigger_error('Invalid query: ' . $con->error);
            }

            if ($res_feature->num_rows) {
              while ($row_feature = $res_feature->fetch_object()) {
            ?>
            <li class="map-input-list-item">
              <div class="map-input-list-checkbox-wrapper w-checkbox">
                <input type="checkbox" id="<?=$row_feature->title?>" class="map-input-list-checkbox w-checkbox-input" value="<?=$row_feature->title?>">
                <label for="<?=$row_feature->title?>" class="map-input-list-checkbox-label w-form-label"><?=$row_feature->title?></label>
              </div>
            </li>
            <?php
              }
            }
            $res_feature->free();
            ?>
          </div>
        </div>
        <input type="submit" value="Go" class="map-btn-search w-button">
      </form>
    </div>
  </div>
  <div class="map-container-map">
    <div class="map-container-properties">
      <a href="#" class="map-btn-close-properties w-inline-block">
        <div>X</div>
      </a>
      <div class="map-container-properties-global">
        <?php
          if (!empty($_POST)) {
            require 'process-search.php';

            $res = $con->query($query);

            if (!$res) {
              trigger_error('Invalid query: ' . $con->error);
            }

            if ($res->num_rows) {
              while ($row = $res->fetch_object()) {
                $main_image = json_decode($row->images);
                $description = get_sub_string($row->description, 150);
            ?>
            <div class="map-item-property">
              <div class="map-container-img-text">
                <div class="map-bg-img-property" style="background-image: url('<?=$main_image->image0?>');"><img src="images/placeholdit_181x111.png" class="map-img-property"></div>
                <div class="map-information-property">
                  <div class="map-title-property"><?=$row->title?></div>
                  <div class="map-container-amenities">
                    <div class="map-item-amenitie">
                      <div class="map-icon-amenitie"><img src="images/Path-45.png"></div>
                      <div class="map-number-amenitie"><?=$row->beds?></div>
                    </div>
                    <div class="map-item-amenitie">
                      <div class="map-icon-amenitie"><img src="images/Path-46.png"></div>
                      <div class="map-number-amenitie"><?=$row->baths?></div>
                    </div>
                    <div class="map-item-amenitie">
                      <div class="map-icon-amenitie"><img src="images/Group-15.png"></div>
                      <div class="map-number-amenitie"><?=$row->parking?></div>
                    </div>
                  </div>
                  <p class="map-text-property"><?=$description?></p>
                </div>
              </div>
              <a href="<?=$row->permalink?>" target="_blank" class="map-link-detail w-inline-block"><img src="images/Group-49.png" class="map-icon-link-detail"></a>
            </div>
            <?php
              }
            }else{
              echo "No results";
            }
            $res->free();
            $con->close();
          }
        ?>
      </div>
    </div>
    <div class="map-container" id="map-container"></div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAW_5khziKZ8cqrqEqadNZ3tDi1nlY5c2o'></script>
  <script src="js/scripts.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>