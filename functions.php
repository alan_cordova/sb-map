<?php
  function get_sub_string($text, $length = 50){
    $text = trim($text);
    if(strlen($text) > $length){
        $text = substr($text, 0, strpos($text, ' ', $length)) . ' ...';
        //$text = substr($text, 0, $length) . ' ...';
    }
    return $text;
  }

?>