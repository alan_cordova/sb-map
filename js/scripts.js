var Webflow = Webflow || [];
Webflow.push(function() {

    var viewport;
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window['safari'] || safari.pushNotification);
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    function get_scroll_top() {
        return $(window).scrollTop();
    }

    function get_viewport() {
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        return {
            w: w,
            h: h
        };
    }

    function close_all_list(){
        $('.map-input-selection').css('display', 'none');
        $('.map-input-selection-type').css('display', 'none');
        $('.map-input-selection-amenities').css('display', 'none');
    }

    function showHide(container, list, other1, other2){
        $(container).on('click', function(){
            if($(list).is(':visible')){
                $(list).fadeOut("fast");
            }else{
                $(list).fadeIn("fast").css("display","flex");
                $(other1).fadeOut("fast");
                $(other2).fadeOut("fast");
            } 
        });
    }

    $( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });

    function get_value_checkbox(item, input, array){
        $(item).change(function(){
            if($(this).is(":checked")) {
                array.push($(this).val());
            }else{
                array.splice($.inArray($(this).val(), array),1);
            }

            $(input).val(array);
        });
    }

    function wrap_items(container, wrapper, limit){
        var items = $(container);
        for(var i = 0; i < items.length; i+=limit) {
          items.slice(i, i+limit).wrapAll(wrapper);
        }
    }

    viewport = get_viewport();
    Webflow.resize.on(function() {
        viewport = get_viewport();
        close_all_list();
        fixSearchCustomMapHeight();
        fixSearchPropertiesTop();
        fixSearchPropertiesHeight();
    });

    function fixSearchCustomMapHeight(){
        if($(".map-container").length > 0){
            $(".map-container").css('height', viewport.h + 'px');
            //$(".map-container").css('top', $(".map-top-container-search").outerHeight() + 'px');
        }
    }

    function fixSearchPropertiesTop(){
        if($(".map-container-properties").length > 0){
            $(".map-container-properties").css('top', 30 + $(".map-top-container-search").outerHeight() + 'px');
        }
    }

    function fixSearchPropertiesHeight(){
        if($(".map-container-properties-global").length > 0){
            var top_properties = $(".map-container-properties").css('top').replace(/\D/g,'');
            $(".map-container-properties-global").css('max-height', viewport.h - top_properties - 120 + 'px');
        }
    }

    var statusArray = [];
    var typeArray = [];
    var amenitiesArray = [];

    wrap_items(".map-input-selection-type li", "<ul class='map-input-list-type'></div>", 4);
    wrap_items(".map-input-selection-amenities li", "<ul class='map-input-list-amenities'></div>", 10);

    get_value_checkbox(".map-input-list input[type='checkbox']",".map-select-status", statusArray);
    get_value_checkbox(".map-input-list-type input[type='checkbox']",".map-select-type", typeArray);
    get_value_checkbox(".map-input-list-amenities input[type='checkbox']",".map-select-amenities", amenitiesArray);

    showHide('.map-select-status', '.map-input-selection', '.map-input-selection-type', '.map-input-selection-amenities');
    showHide('.map-select-type', '.map-input-selection-type', '.map-input-selection', '.map-input-selection-amenities');
    showHide('.map-select-amenities', '.map-input-selection-amenities', '.map-input-selection', '.map-input-selection-type');
    
});