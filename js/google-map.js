(function($) {
    function new_map( $el ) {
        var $markers = $el.find('.marker');
        var latLng = {
            lat: parseFloat(20.664348833879615),
            lng: parseFloat(-105.25233127176762)
        };
        var args = {
            zoom        : 14,
            scrollwheel: false,
            center      : new google.maps.LatLng(latLng.lat, latLng.lng),
            mapTypeId   : google.maps.MapTypeId.ROADMAP
        };             
        var map = new google.maps.Map( $el[0], args);
        map.markers = [];
        $markers.each(function(){
            add_marker( $(this), map );
        });
        center_map( map );
        return map;
    }
    function add_marker( $marker, map ) {
        var image = THEME_PATH + $marker.attr('data-marker');
        var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
        var marker = new google.maps.Marker({
            position    : latlng,
            map         : map,
            icon        : image
        });
        map.markers.push( marker );
        if( $marker.html() )
        {
            var infowindow = new google.maps.InfoWindow({
                content     : $marker.html(),
                maxWidth: 200
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open( map, marker );
            });
        }
    }
    function resize_center(){
        var view_width = $( window ).width();
        if ( view_width >= 769 ) {
            map.setZoom(13);
        }else if( view_width <= 768 ){
            map.setZoom(13);
        }
    }
    function center_map( map ) {
        var bounds = new google.maps.LatLngBounds();
        $.each( map.markers, function( i, marker ){
            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
            bounds.extend( latlng );
        });
        if( map.markers.length == 1 )
        {
            map.setCenter( bounds.getCenter() );
            map.setZoom( 14 );
        }
        else
        {
            google.maps.event.addListener(map, 'zoom_changed', function() {
                zoomChangeBoundsListener = google.maps.event.addListener(map, 'bounds_changed', function(event) {
                if (map.getZoom() > 10 && map.initialZoom == true) {
                    resize_center();
                    $(window).on('resize', function(){
                        resize_center();
                    });
                    map.initialZoom = false;
                }
                    google.maps.event.removeListener(zoomChangeBoundsListener);
                });
            });
            map.initialZoom = true;
            map.fitBounds( bounds );
        }
        google.maps.event.addDomListener(window, "resize", function() {
                google.maps.event.trigger(map, "resize");
                map.setCenter( bounds.getCenter() );
        });
    }
    var map = null;
    $(document).ready(function(){
        $('.map-container').each(function(){
            map = new_map( $(this) );
        });
    });
})(jQuery);